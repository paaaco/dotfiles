set nocompatible              " be iMproved, required
filetype off                  " required
set hidden
let mapleader = ','
au Filetype python setl et ts=4 sw=4 " for python spaces
let g:indentLine_enabled = 0

set clipboard^=unnamedplus,unnamed " share clipboard for vim + system
set backspace=indent,eol,start  " more powerful backspacing
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'itchyny/lightline.vim'
Plugin 'mengelbrecht/lightline-bufferline'
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
Plugin 'chrisbra/csv.vim'
Plugin 'junegunn/goyo.vim'
Plugin 'sheerun/vim-polyglot'
Plugin 'mhinz/vim-startify'
Plugin 'Yggdroot/indentLine'
Plugin 'airblade/vim-gitgutter'
"Plugin 'yuratomo/w3m.vim'
" Syntax highlighters
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'
"Plugin 'arcticicestudio/nord-vim'
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Netrw settings. displayed in tree view and without top banner
let g:netrw_banner = 0
let g:netrw_winsize = 15
let g:netrw_browse_split = 4

syntax on
set number relativenumber
highlight LineNr ctermfg=White ctermbg=DarkBlue
highlight LineNrBelow ctermfg=Brown
highlight LineNrAbove ctermfg=Brown

let g:gitgutter_override_sign_column_highlight = 0
highlight SignColumn ctermbg=white
highlight GitGutterAdd ctermfg=green
highlight GitGutterDelete ctermfg=red
highlight GitGutterChange ctermfg=blue

" syntax folding
set foldmethod=indent
nnoremap <Space> za
set foldlevel=99

" Highlight searching
set hls
set incsearch
hi Search ctermfg=white

" Use a blinking upright bar cursor in Insert mode, a blinking block in normal
if &term == 'xterm-256color' || &term == 'screen-256color'
    let &t_SI = "\<Esc>[5 q"
    let &t_EI = "\<Esc>[1 q"
endif

if exists('$TMUX')
    let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
    let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
endif

set laststatus=2

set timeout timeoutlen=3000 ttimeoutlen=100

set expandtab
set tabstop=2
set shiftwidth=2
set noshowmode
let &t_SI = "\<Esc>]50;CursorShape=2\x7"
let &t_EI = "\<Esc>]50;CursorShape=0\x7"

set showtabline=2
let g:lightline = {
  \'colorscheme': 'PaperColor_light',
  \'active': { 
    \'right': [ 
      \['lineinfo'],
      \['percent'],
      \['filetype']
    \]
  \},
  \'tabline': {
    \'left':[ ['buffers'] ],
  \},
  \'component_expand': {
    \'buffers': 'lightline#bufferline#buffers',
  \},
  \'component_type': {
    \'buffers': 'tabsel',
  \}
\}

let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'Comment'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

" vim-startify configuration
let g:startify_lists = [
      \ { 'type': 'sessions',  'header': ['   Sessions']       },
      \ { 'type': 'files',     'header': ['   MRU'] }
      \ ]
let g:startify_session_persistence = 1
let g:startify_session_autoload = 1
let g:startify_enable_special = 0
let g:startify_custom_header = 'startify#fortune#quote()'
" autocmd VimLeave * :SSave

" Shortcuts for moving between buffers.
noremap <C-j> :bp<CR>
noremap <C-k> :bn<CR>
noremap <Leader>w :bd<CR>

nmap <Leader>ev :e $MYVIMRC<CR>
nmap <Leader>sv :so $MYVIMRC<CR>

map <C-p> :Files<CR>
map <C-o> :Buffer<CR>

nmap <Leader>a :Ag<Space>
nnoremap <silent> <C-a> :Ag <C-R><C-W><CR>

" map d to delete without copying
nnoremap d "_d
vnoremap d "_d

" clear search highlight on enter
noremap <CR> :noh<CR>

" Goyo stuff
function! s:goyo_enter()
    " silent! call lightline#enable()
    highlight StatusLineNC ctermfg=white 
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested set eventignore=

" autocmd VimEnter * Goyo 80x100%" 
" command Goyoo :Goyo <bar> highlight StatusLineNC ctermfg=white

"
map <C-n> :Lex<CR> 

" toggle all numbers
map <C-l> :set number! relativenumber!<CR>

let &t_ZH="\e[3m"
let &t_ZR="\e[23m"
highlight Comment cterm=italic

